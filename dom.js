/**
 * removeAllSpans -  функция, которая удаляет все элементы span со страницы
 */

function removeAllSpans () {
	var i,
		collectionSpan = document.querySelectorAll('span');

	for (i = 0; i < collectionSpan.length; i++) {
		collectionSpan[i].parentNode.removeChild(collectionSpan[i]);
	}
}

removeAllSpans();


/**
 * firstChild -  функция, которая будет кроссбразурено возвращать firstChild (не текстовую ноду).
 *
 * @param {Node} parent node
 * @return {Node}
 */

function firstChild (parent) {
	var i, node,
		collectionNodes = parent.childNodes,
		length = collectionNodes.length;

	
	for (i = 0; i < length; i++) {
		node = exceptTextNode(collectionNodes[i]);

		if (node) {
			return node;
		}
	}
}

var firstChild = firstChild( document.querySelector('div') );

console.log(firstChild);


/**
 * lastChild -  функция, будет кроссбраузерно возвращать lastChild (не текстовую ноду).
 *
 * @param {Node} parent node
 * @return {Node}
 */

function lastChild (parent) {
	var i, node,
		collectionNodes = parent.childNodes,
		length = collectionNodes.length;
	
	for (i = length - 1; i > 0; i--) {
		node = exceptTextNode(collectionNodes[i]);

		if (node) {
			return node;
		}
	}
}

var lastChild = lastChild( document.querySelector('div') );

console.log(lastChild);


/**
 * next -  функция, которая будет кроссбраузерно возвращать следующий элемент
 * (не текстовую ноду, работа наподобие next() в jQuery).
 *
 * @param {Node} node
 * @return {Node}
 */

function next (node) {
	var i, nextNode,
		nodeParent = node.parentNode,
		children = nodeParent.childNodes,
		index = findIndexNode(node);
	
	for (i = index + 1; i < children.length; i++) {
		nextNode = exceptTextNode(children[i]);
		
		if (nextNode) {
			return nextNode;
		}
	}
}

var next = next( document.querySelector('aside') );

console.log(next);


/**
 * prev -  функция, которая будет кроссбраузерно возвращать предыдущий элемент
 * (не текстовую ноду, работа наподобие prev() в jQuery).
 *
 * @param {Node} node
 * @return {Node}
 */

function prev (node) {
	var i, prevNode, 
		nodeParent = node.parentNode,
		children = nodeParent.childNodes,
		index = findIndexNode(node);

	for (i = index - 1; i > 0; i--) {
		prevNode = exceptTextNode(children[i]);

		if (prevNode) {
			return prevNode;
		}
	}
}

var prev = prev( document.querySelector('.next') );

console.log(prev);


/**
 * OPTIONAL
 * closest -  функция, которая будет идти вверх по DOM (по родительским елементам) от переданного ей элемента в первом
 * аргументе пока не найдет элемент переданный во втором аргументе в эту функцию и вернёт этот элемент (то есть второй).
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/closest
 *
 * Example:
 * closest(document.querySelector('[data-url="http://google.ru"]'), 'aside'); // return <aside>...</aside>
 *
 * @param {Node} node
 * @param {String} selector
 * @return {Node}
 */

function closest (node, selector) {
	// todo
}


/* exceptTextNode */
function exceptTextNode (node) {
	if (node.nodeType != 3) {
		return node;
	}

	return false;
}

/* findIndexNode */
function findIndexNode (node) {
	var i, index;
		nodeParent = node.parentNode,
		children = nodeParent.childNodes,
		length = children.length;

	for (i = 0; i < length; i++) {
		if (children[i] === node) {
			return index = i;
		}
	}
}